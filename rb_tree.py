from binarytree import Node


class RBTree(Node):
    def __rb_insert_fixup(self, z):
        raise NotImplementedError()

    def insert(self, node):
        y = None
        x = self
        while not x == None:
            y = x
            if node.value < x.value:
                x = x.left
            else:
                x = x.right
        node.p = y
        if node.value < y.value:
            y.left = node
        else:
            y.right = node
        node.color = 'RED'
        # self.__rb_insert_fixup(node)


if __name__ == '__main__':
    import random
    root = RBTree(50)
    for x in range(1, 10):
        root.insert(Node(
            random.choice(
                range(1, 100))))
    print(root)
